﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static TimeSpan REFRESH_RATE = TimeSpan.FromMilliseconds(250);
        static DateTime lastRefresh;
        static Game game;

        static void LoadDLL(Game g)
        {
            string[] plugins = Directory.GetFiles(Environment.CurrentDirectory + "\\plugins", "*.dll");
            
            foreach (string dll in plugins)
            {
                Assembly assembly = Assembly.LoadFile(dll);
                foreach (Type t in assembly.GetTypes())
                {
                    if (t.GetInterface("IMovableItem") != null)
                    {
                        Console.WriteLine(">>> LOADING " + dll);
                        object[] file = new object[]
                        {
                            (dll.Trim('d', 'l', 'l') + "cfg")
                        };
                        object instance = Activator.CreateInstance(t, file);
                        g.AddPlayer(instance as IMovableItem);
                        Console.WriteLine("LOADED");
                    }
                }
            }
            Console.WriteLine("\nPRESS ANY KEY TO START");
            Console.ReadKey();
        }

        static void Main(string[] args)
        {   
            Console.CursorVisible = false;
            game = new Game(15, 15);
            LoadDLL(game);
            Console.Clear();
            game.ItemChanged += RefreshMap;
            DrawInitialMap();

            while (!game.IsGameOver)
            {
                game.Tick();
                Thread.Sleep(250);
            }

            Console.ReadKey();
        }

        static void DrawInitialMap()
        {
            //Console.BackgroundColor = ConsoleColor.DarkGray;
            for (int y = 0; y < game.Map.Y; y++)
            {
                string s = new string(' ', game.Map.X);
                Console.WriteLine(s);
            }
            lastRefresh = DateTime.Now;
        }

        static void RefreshMap(object sender, EventArgs args)
        {
            if (DateTime.Now > lastRefresh.Add(REFRESH_RATE))
            {
                ItemChangedEventArgs arg = args as ItemChangedEventArgs;

                if (arg.NewItem == null) // Remove item
                {
                    Console.SetCursorPosition(arg.OldItem.Position.X, arg.OldItem.Position.Y);
                    Console.Write(' ');
                }
                else
                {
                    if (arg.OldItem == null) // New item
                    {
                        dynamic it = null;
                        if (arg.NewItem is MovableItem)
                            it = arg.NewItem as MovableItem;
                        if (it.Item is IColorable)
                        {
                            IColorable colorItem = it.Item as IColorable;
                            Console.ForegroundColor = colorItem.Color;
                        }

                        Console.SetCursorPosition(arg.NewItem.Position.X, arg.NewItem.Position.Y);

                        if (arg.NewItem is MovableItem)
                        {
                            MovableItem item = arg.NewItem as MovableItem;
                            Console.Write(item.Item.HeadChar);

                            foreach (Coordinate c in item.SubPositions)
                            {
                                Console.SetCursorPosition(c.X, c.Y);
                                Console.Write(item.Item.BodyChar);
                            }
                        }
                        else if (arg.NewItem is StaticItem)
                        {
                            StaticItem item = arg.NewItem as StaticItem;
                            Console.Write(item.Item.ItemChar);
                        }
                    }
                    else // Item moved
                    {
                        MovableItem oldItem = arg.OldItem as MovableItem;
                        MovableItem newItem = arg.NewItem as MovableItem;

                        foreach (Coordinate c in oldItem.SubPositions)
                        {
                            Console.SetCursorPosition(c.X, c.Y);
                            Console.Write(' ');
                        }
                        Console.SetCursorPosition(oldItem.Position.X, oldItem.Position.Y);
                        Console.Write(' ');

                        foreach (Coordinate c in newItem.SubPositions)
                        {
                            Console.SetCursorPosition(c.X, c.Y);
                            Console.Write(newItem.Item.BodyChar);
                        }
                        Console.SetCursorPosition(newItem.Position.X, newItem.Position.Y);
                        Console.Write(newItem.Item.HeadChar);
                    }
                }
                lastRefresh = DateTime.Now;
            }
        }
    }
}
