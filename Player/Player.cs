﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Player
{
    public class Player : IMovableItem, IColorable
    {
        [DllImport("User32.dll")]
        public static extern short GetAsyncKeyState(int vKey);

        public char HeadChar { get; private set; }
        public char BodyChar { get; private set; }
        public ConsoleColor Color { get; private set; }
        public int ElementsToAdd { get; set; }
        public Coordinate NextMove { get; private set; }

        Dictionary<Key, Coordinate> Movements;
        Dictionary<string, string> DefaultConfig;
        Dictionary<string, string> Config;

        bool IsCustomConfiguration
        {
            get
            {
                bool isCustom = false;
                foreach(var cfg in DefaultConfig)
                {
                    if (!Config.ContainsKey(cfg.Key) || Config[cfg.Key] != cfg.Value)
                    {
                        isCustom = true;
                        break;
                    }
                }
                return isCustom;
            }
        }

        public Player(string configFile)
        {
            SetDefaultConfiguration();
            NextMove = MoveDirection.None;
            LoadConfiguration(configFile);

            if (IsCustomConfiguration)
                SaveConfiguration(configFile);
        }

        void SetDefaultConfiguration()
        {
            DefaultConfig = new Dictionary<string, string>();
            DefaultConfig.Add("LEFT", "Left");
            DefaultConfig.Add("RIGHT", "Right");
            DefaultConfig.Add("UP", "Up");
            DefaultConfig.Add("DOWN", "Down");
            DefaultConfig.Add("HEAD_CHAR", "O");
            DefaultConfig.Add("BODY_CHAR", "o");
            DefaultConfig.Add("CONSOLE_COLOR", "Gray");
        }

        void LoadConfiguration(string configFile)
        {
            SetDefaultConfiguration();
            try
            {
                StreamReader reader = new StreamReader(configFile);
                string[] cfg = reader.ReadToEnd().Split('\n');
                reader.Close();
                Movements = new Dictionary<Key, Coordinate>();
                Config = new Dictionary<string, string>();
                foreach(string line in cfg)
                {
                    string[] token = line.Split('=');

                    if (DefaultConfig.ContainsKey(token[0]))
                        Config.Add(token[0], token[1]);
                }
                ApplyConfiguration();
            }
            catch (FileNotFoundException)
            {
                Config = DefaultConfig;
                ApplyConfiguration();
            }
        }

        void ApplyConfiguration()
        {
            Dictionary<string, string> Conf = new Dictionary<string, string>();

            foreach(var cfg in DefaultConfig)
            {
                if (Config.ContainsKey(cfg.Key))
                    Conf.Add(cfg.Key, Config[cfg.Key]);
                else
                    Conf.Add(cfg.Key, cfg.Value);
            }

            Movements.Add((Key)Enum.Parse(typeof(Key), Conf["LEFT"], true), MoveDirection.Left);
            Movements.Add((Key)Enum.Parse(typeof(Key), Conf["RIGHT"], true), MoveDirection.Right);
            Movements.Add((Key)Enum.Parse(typeof(Key), Conf["UP"], true), MoveDirection.Up);
            Movements.Add((Key)Enum.Parse(typeof(Key), Conf["DOWN"], true), MoveDirection.Down);
            HeadChar = Conf["HEAD_CHAR"][0];
            BodyChar = Conf["BODY_CHAR"][0];
            Color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), Conf["CONSOLE_COLOR"], true);
        }

        void SaveConfiguration(string configFile)
        {
            StreamWriter writer = new StreamWriter(configFile);
            StringBuilder sb = new StringBuilder();

            foreach (var cfg in DefaultConfig)
            {
                if (Config.ContainsKey(cfg.Key))
                    sb.AppendLine(cfg.Key + "=" + Config[cfg.Key]);
                else
                    sb.AppendLine(cfg.Key + "=" + cfg.Value);
            }

            writer.Write(sb);
            writer.Close();
        }

        public void ProcessMovement()
        {
            foreach (var m in Movements)
            {
                int vkey = KeyInterop.VirtualKeyFromKey(m.Key);

                if (GetAsyncKeyState(vkey) != 0)
                {
                    if ((m.Value.X != 0 && Math.Abs(m.Value.X) != Math.Abs(NextMove.X)) || (m.Value.Y != 0 && Math.Abs(m.Value.Y) != Math.Abs(NextMove.Y)))
                        NextMove = m.Value;
                }
            }
        }

        public void Move()
        {

        }
    }
}
