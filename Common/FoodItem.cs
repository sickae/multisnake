﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class FoodItem : StaticItem
    {
        public FoodItem(IStaticItem food)
        {
            Item = food;
        }
    }
}
