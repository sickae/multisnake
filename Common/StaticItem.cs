﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class StaticItem : GameItem
    {
        public IStaticItem Item { get; set; }

        public override Coordinate Position { get; set; }
    }
}
