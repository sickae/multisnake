﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class MovableItem : GameItem
    {
        public IMovableItem Item { get; set; }
        public List<Coordinate> SubPositions { get; set; }

        public override Coordinate Position { get; set; }
    }
}
