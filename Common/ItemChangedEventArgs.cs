﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ItemChangedEventArgs : EventArgs
    {
        public GameItem OldItem { get; private set; }
        public GameItem NewItem { get; private set; }

        public ItemChangedEventArgs(GameItem oldItem, GameItem newItem)
        {
            this.OldItem = oldItem;
            this.NewItem = newItem;
        }
    }
}
