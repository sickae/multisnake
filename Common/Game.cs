﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common
{
    public class Game
    {
        const int ITEM_REMOVAL_INTERVAL = 3000;

        static object itemLock = new object();
        static Random rnd = new Random();

        public event EventHandler ItemChanged;

        public bool IsGameOver { get => !items.Any(x => x is MovableItem); }
        public MapSize Map { get; private set; }
        public List<GameItem> Items { get => items; }

        List<GameItem> items;
        Dictionary<GameItem, DateTime> removable; // GameItem, removalTime
        List<Coordinate> freePos;
        int foodNum;

        public struct MapSize
        {
            public int X;
            public int Y;

            public MapSize(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }
        }

        public Game(int x, int y)
        {
            items = new List<GameItem>();
            removable = new Dictionary<GameItem, DateTime>();
            Map = new MapSize(x, y);
        }

        void RefreshFreePositions()
        {
            freePos?.Clear();
            freePos = new List<Coordinate>();

            for (int x = 0; x < Map.X; x++)
            {
                for (int y = 0; y < Map.Y; y++)
                {
                    freePos.Add(new Coordinate(x, y));
                }
            }

            foreach (GameItem item in items)
            {
                freePos.Remove(freePos.SingleOrDefault(x => x.X == item.Position.X && x.Y == item.Position.Y));

                if (item is MovableItem)
                {
                    MovableItem it = item as MovableItem;
                    foreach(Coordinate c in it.SubPositions)
                    {
                        freePos.Remove(freePos.SingleOrDefault(x => x.X == c.X && x.Y == c.Y));
                    }
                }
            }
        }

        public void AddPlayer(IMovableItem item)
        {
            MovableItem newItem = new MovableItem();
            newItem.Item = item;
            newItem.Position = new Coordinate(5, 5); // TODO: starting position
            newItem.SubPositions = new List<Coordinate>();
            items.Add(newItem);
        }

        public void Tick()
        {
            lock (itemLock)
            {
                foreach (GameItem it in items)
                {
                    if (it is MovableItem)
                    {
                        MovableItem i = it as MovableItem;
                        if (!removable.ContainsKey(i) && !DetectCollision(i))
                        {
                            i.Item.ProcessMovement();
                            MoveItem(i);
                        }
                    }
                }
            }
            Task.Run(() => RemoveItems());

            if (foodNum < items.Where(x => x is MovableItem).Count())
                GenerateFood();
        }

        void GenerateFood()
        {
            lock (itemLock)
            {
                RefreshFreePositions();
                if (freePos.Count > 0)
                {
                    int idx = rnd.Next(0, freePos.Count);
                    FoodItem f = new FoodItem(new Food());
                    f.Position = new Coordinate(freePos[idx].X, freePos[idx].Y);
                    items.Insert(0,f);
                    foodNum++;
                    ItemChanged?.Invoke(this, new ItemChangedEventArgs(null, f));
                }
            }
        }

        bool DetectCollision(MovableItem item)
        {
            if (item.Item.NextMove.X == 0 && item.Item.NextMove.Y == 0)
                return false;

            int x = (Map.X + item.Position.X + item.Item.NextMove.X) % Map.X;
            int y = (Map.Y + item.Position.Y + item.Item.NextMove.Y) % Map.Y;

            foreach (GameItem it in items)
            {
                if (it.Position.X == x && it.Position.Y == y)
                {
                    if (it is FoodItem)
                    {
                        if (!removable.ContainsKey(it))
                        {
                            removable.Add(it, DateTime.Now);
                            item.Item.ElementsToAdd++;
                            foodNum--;
                        }
                    }
                    else
                    {
                        if (!removable.ContainsKey(item))
                            removable.Add(item, DateTime.Now.AddSeconds(3));

                        return true;
                    }
                }

                if (it is MovableItem)
                {
                    MovableItem i = it as MovableItem;
                    foreach (Coordinate c in i.SubPositions)
                    {
                        if (c.X == x && c.Y == y)
                        {
                            if (!removable.ContainsKey(item))
                                removable.Add(item, DateTime.Now.AddSeconds(3));

                            return true;
                        }
                    }
                }




                //if (it is MovableItem)
                //{
                //    MovableItem i = it as MovableItem;
                //    foreach (Coordinate c in i.SubPositions)
                //    {
                //        if (c.X == x && c.Y == y)
                //        {
                //            if (!removable.ContainsKey(item))
                //                removable.Add(item, DateTime.Now.AddSeconds(3));

                //            return true;
                //        }
                //    }
                //}
                //else if (it is StaticItem)
                //{
                //    StaticItem i = it as StaticItem;
                //    if (i.Position.X == x && i.Position.Y == y)
                //    {
                //        if (it is FoodItem)
                //        {
                //            if (!removable.ContainsKey(i))
                //            {
                //                removable.Add(i, DateTime.Now);
                //                item.Item.ElementsToAdd++;
                //                foodNum--;
                //            }
                //        }
                //    }
                //}
            }

            return false;
        }

        void RemoveItems()
        {
            lock (itemLock)
            {
                List<GameItem> temp = new List<GameItem>();
                temp.AddRange(removable.Where(x => DateTime.Now > x.Value).Select(x => x.Key));

                foreach (GameItem item in temp)
                {
                    removable.Remove(item);
                    items.Remove(item);
                    ItemChanged?.Invoke(this, new ItemChangedEventArgs(item, null));
                }
            }
        }

        void MoveItem(MovableItem item)
        {

            MovableItem temp = new MovableItem()
            {
                Position = new Coordinate(item.Position.X, item.Position.Y),
                SubPositions = item.SubPositions.ToList()
            };
            item.Position.X = (Map.X + item.Position.X + item.Item.NextMove.X) % Map.X;
            item.Position.Y = (Map.Y + item.Position.Y + item.Item.NextMove.Y) % Map.Y;
            int prevX = item.Position.X;
            int prevY = item.Position.Y;

            for (int i = 0; i < item.SubPositions.Count; i++)
            {
                int x = item.SubPositions[i].X;
                int y = item.SubPositions[i].Y;
                item.SubPositions[i].X = (Map.X + prevX) % Map.X;
                item.SubPositions[i].Y = (Map.Y + prevY) % Map.Y;
                prevX = x;
                prevY = y;
            }

            if (item.Item.ElementsToAdd > 0)
            {
                item.SubPositions.Add(new Coordinate((Map.X + prevX) % Map.X, (Map.Y + prevY) % Map.Y));
                item.Item.ElementsToAdd--;
            }

            ItemChanged?.Invoke(this, new ItemChangedEventArgs(temp, item));
        }
    }
}
