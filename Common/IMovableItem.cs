﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IMovableItem
    {
        char HeadChar { get; }
        char BodyChar { get; }
        int ElementsToAdd { get; set; }
        Coordinate NextMove { get; }

        void ProcessMovement();
    }
}
