﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class MoveDirection
    {
        public static Coordinate Left { get => new Coordinate(-1, 0); }
        public static Coordinate Right { get => new Coordinate(1, 0); }
        public static Coordinate Up { get => new Coordinate(0, -1); }
        public static Coordinate Down { get => new Coordinate(0, 1); }
        public static Coordinate None { get => new Coordinate(0, 0); }
    }
}