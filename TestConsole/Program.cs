﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            char[,] buffer = new char[15, 15];
            Test(buffer);

            Console.ReadKey();
        }

        static void Test(char[,] buffer)
        {
            for (int x = 0; x < buffer.GetLength(0); x++)
            {
                for (int y = 0; y < buffer.GetLength(1); y++)
                {
                    Console.SetCursorPosition(x, y);
                    Console.Write(buffer[x,y]);
                }
            }
        }
    }
}
